package com.example.spring.tutorial.service.dto;

import com.example.spring.tutorial.exception.ResourceException;
import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Base service for CRUD operation.
 *
 * @param <E>  Entity class
 * @param <T>  DTO class
 * @param <ID> Id class
 */
public abstract class CrudBaseService<E, T, ID extends Serializable>
        implements BasicCrud<T, ID>, DtoEntityMapping<T, E> {

    private final Class<T> dtoClazz;

    private final Class<E> entityClazz;

    private final ModelMapper mapperInstance;

    public CrudBaseService(Class<E> entityClazz, Class<T> dtoClazz) {
        this.entityClazz = entityClazz;
        this.dtoClazz = dtoClazz;
        mapperInstance = new ModelMapper();
        mapperInstance.getConfiguration().setAmbiguityIgnored(true);
    }

    /**
     * Get corresponding repository.
     *
     * @return a repository
     */
    protected abstract JpaRepository<E, ID> getRepository();

    /**
     * Get id of an entity.
     *
     * @param entity the given entity
     * @return a id of entity
     */
    protected abstract ID getId(E entity);

    @Override
    public List<T> getAll() {
        List<E> entities = getRepository().findAll();
        return convertToDTO(entities);
    }

    @Override
    public T getById(ID id) throws ResourceException {
        E entity = getRepository().findById(id).orElse(null);
        if (entity == null) {
            throw new ResourceException().setParam("id", String.valueOf(id));
        }
        return convertToDTO(entity);
    }

    @Override
    public T create(T object) {
        E entity = convertToEntity(object);
        return convertToDTO(getRepository().save(entity));
    }

    @Override
    public T update(T object) throws Exception {
        E newEntity = convertToEntity(object);
        E existedEntity = getRepository().findById(getId(newEntity)).orElse(null);
        if (existedEntity == null) {
            throw new ResourceException()
                    .setParam("objectType", newEntity.getClass().toString())
                    .setParam("id", getId(newEntity).toString());
        }
        return convertToDTO(getRepository().save(newEntity));
    }

    @Override
    public List<T> add(List<T> objects) {
        List<T> obj = new ArrayList<>();
        for (T object : objects) {
            E newEntity = convertToEntity(object);
            obj.add(convertToDTO(getRepository().save(newEntity)));
        }
        return obj;
    }

    @Override
    public void delete(ID objectId) {
        getRepository().deleteById(objectId);
    }

    @Override
    public boolean exists(ID id) {
        return getRepository().existsById(id);
    }

    @Override
    public T convertToDTO(E entity) {
        return mapperInstance.map(entity, dtoClazz);
    }

    @Override
    public E convertToEntity(T dto) {
        return mapperInstance.map(dto, entityClazz);
    }

    public List<T> convertToDTO(Collection<E> entities) {
        return entities.stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }
}