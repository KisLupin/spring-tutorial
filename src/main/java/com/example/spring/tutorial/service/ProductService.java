package com.example.spring.tutorial.service;

import com.example.spring.tutorial.model.dto.ProductDto;
import com.example.spring.tutorial.model.entity.Product;
import com.example.spring.tutorial.repository.ProductRepository;
import com.example.spring.tutorial.service.dto.CrudBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class ProductService extends CrudBaseService<Product, ProductDto, Integer> {

    @Autowired
    private ProductRepository productRepository;

    public ProductService() {
        super(Product.class, ProductDto.class);
    }

    @Override
    protected JpaRepository<Product, Integer> getRepository() {
        return productRepository;
    }

    @Override
    protected Integer getId(Product entity) {
        return entity.getId();
    }
}
