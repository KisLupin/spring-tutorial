package com.example.spring.tutorial.service.dto;

/**
 * This interface provides basic operations for mapping from entity to DTO and reverse.
 */
public interface DtoEntityMapping<T, E> {

    /**
     * Convert an entity to a model.
     *
     * @param entity the given entity
     * @return a model
     */
    T convertToDTO(E entity);

    /**
     * Convert a model to an entity.
     *
     * @param dto the given model
     * @return an entity
     */
    E convertToEntity(T dto);
}

