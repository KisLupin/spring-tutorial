package com.example.spring.tutorial.service.dto;

import com.example.spring.tutorial.exception.ResourceException;

import java.util.List;

/**
 * This interface provides basic CRUD operations.
 */
public interface BasicCrud<T, ID> {

    /**
     * Retrieve all entity.
     *
     * @return a list of entity
     */
    List<T> getAll();

    /**
     * Retrieve an existing entity by id.
     *
     * @param id the given object id
     * @return an existing entity
     */
    T getById(ID id) throws ResourceException;

    /**
     * Save a new entity to database.
     *
     * @param object the given model
     * @return inserted object
     */
    T create(T object) throws Exception;

    /**
     * Update an existed entity in database.
     *
     * @param object the given model
     * @return updated object
     */
    T update(T object) throws Exception;

    /**
     * add an entity in database.
     *
     * @param object the given model
     * @return add object
     */
    List<T> add(List<T> object) throws Exception;

    /**
     * Delete an existed entity in database.
     *
     * @param objectId the given object id
     */
    void delete(ID objectId) throws Exception;

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param id must not be {@literal null}.
     * @return true if an entity with the given id exists, {@literal false} otherwise
     * @throws IllegalArgumentException if {@code id} is {@literal null}
     */
    boolean exists(ID id);
}