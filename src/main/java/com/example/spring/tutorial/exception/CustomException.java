package com.example.spring.tutorial.exception;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * This class defines the base exception for this project.
 */
@Getter
public class CustomException extends Exception {
    protected CustomError error;

    private static final String PARAM_MESSAGE = "%s = %s";
    private static final String PARAM_SEPARATOR = ", ";


    public CustomException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.error = new CustomError(errorCode.getId(), errorCode.getMessage(), new HashMap<>());
    }

    public CustomException(ErrorCode errorCode, Throwable cause) {
        super(errorCode.getMessage(), cause);
        this.error = new CustomError(errorCode.getId(), errorCode.getMessage(), new HashMap<>());
    }

    public CustomException setParam(String name, String value) {
        this.error.getParams().put(name, value);
        return this;
    }

    @Override
    public String getMessage() {
        return this.buildMessage(super.getMessage());
    }

    @Override
    public String getLocalizedMessage() {
        return this.buildMessage(super.getLocalizedMessage());
    }

    private String buildMessage(String initialString) {
        StringBuilder stringBuilder = new StringBuilder(initialString);
        for (Map.Entry<String, String> entry : this.error.getParams().entrySet()) {
            String paramMessage = String.format(PARAM_MESSAGE, entry.getKey(), entry.getValue());
            stringBuilder.append(PARAM_SEPARATOR);
            stringBuilder.append(paramMessage);
        }
        stringBuilder.append(", businessErrorCode = ".concat(error.getCode()));
        return stringBuilder.toString();
    }
}
