package com.example.spring.tutorial.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

/**
 * This class defines the error information for this project.
 */
@Getter
@AllArgsConstructor
public class CustomError {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private String code;
    private String message;
    private Map<String, String> params;

    public String buildCosmosError() {
        ObjectNode node = OBJECT_MAPPER.createObjectNode();
        node.put("code", code);
        node.put("message", message);
        node.putPOJO("params", this.buildParamNode());
        return node.toString();
    }

    private ArrayNode buildParamNode() {
        ArrayNode paramNodes = OBJECT_MAPPER.createArrayNode();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            paramNodes.add(OBJECT_MAPPER.createObjectNode().put(entry.getKey(), entry.getValue()));
        }
        return paramNodes;
    }
}
