package com.example.spring.tutorial.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Exception CosmosError Codes.
 */
@AllArgsConstructor
@Getter
public enum ErrorCode {
    RESOURCE_NOT_FOUND("E00001", "Resource could not be found"),
    WF_DATA_NOT_FOUND("E00002", "Workfront Data not found"),
    INVALID_PARAMETER_EXCEPTION("E00003", "Invalid parameter exception"),
    INVALID_DATA_XML_MAPPING_LEVEL("E00004", "Invalid data XML mapping level"),
    P8_DATA_NOT_FOUND("E00005", "P8 Data not found"),
    UNSUPPORTED_FILE_FORMAT("E00006", "Document with given template not found"),
    PDFC_EXCEPTION("E00007", "Pdfc exception"),
    SC_FAILED_HTTP("E00008", "Failed to request to smart com service via http"),
    FAILED_DESERIALIZE_JSON("E00009", "Failed to deserialize json value"),
    FAILED_SERIALIZE_JSON("E00010", "Failed to serialize object to json value"),
    GENERIC_PROBLEM("E00011", "Unexpected exception"),
    UNAUTHORIZED("E00012", "Unauthorized"),
    FAILED_TO_CREATE_RESPONSE("E00013", "Failed to create response"),
    REST_API_EXCEPTION("E00014", "Failed to call external API"),
    TEMPLATE_NOT_FOUND("E00015", "Template not found - %s"), //reason + ref (ID/name)
    DATA_XML_MAPPING_NOT_FOUND("E00016", "Data XML Mapping not found - %s"), //reason + ref (ID/name)
    DATA_XML_MAPPING_LEVEL_INVALID("E00017", "Data XML Mapping Level invalid - %s"), //reason + ref (ID/name)
    FAILED_EXTRACTION_VALUE("E00018", "Failed to extract value - %s"),
    FAILED_EVALUATION_REPRESENTATION("E00019", "Failed to evaluate representation - %s"),
    RESOURCE_EXIST("E00020", "Resource already exist")
    ;

    private final String id;
    private final String message;

    public String getId() {
        return this.id;
    }

    public String getMessage() {
        return this.message;
    }
}
