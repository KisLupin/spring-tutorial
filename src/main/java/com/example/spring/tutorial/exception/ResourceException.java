package com.example.spring.tutorial.exception;

public class ResourceException extends CustomException {

    public ResourceException() {
        super(ErrorCode.RESOURCE_NOT_FOUND);
    }

    public ResourceException(ErrorCode errorCode) {
        super(errorCode);
    }

    public ResourceException(ErrorCode errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    public ResourceException setParam(String name, String value) {
        this.error.getParams().put(name, value);
        return this;
    }
}
