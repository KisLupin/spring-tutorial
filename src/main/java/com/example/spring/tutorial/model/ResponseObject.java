package com.example.spring.tutorial.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ResponseObject {
    private String code;
    private String message;
    private Object data;

    public ResponseObject(String code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
