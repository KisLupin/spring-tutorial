package com.example.spring.tutorial.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDto {
    private Integer id;

    private String name;

    private Integer number;

    private Double price;

    private String dateOfManufacture;
}
