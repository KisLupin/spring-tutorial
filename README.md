# spring-tutorial



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Lesson 1

Create connection to db and create API with CRUD

## Lesson 2

Create table with constraint many to one, one to many, many to many and create API with CRUD

## Lesson 3

Use paging data

## Lesson 4

Use validation and handle exception
